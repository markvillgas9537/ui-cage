import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root',
})

export class AppService {
    public getAuthMember() {
        return of({
            "status": "SUCCESS",
            "correlationId": 0,
            "message": "get all member success",
            "data": [
              {
                "createdDate": "2021-03-22T17:01:44.290+0800",
                "lastModifiedDate": "2021-11-05T14:51:14.232+0800",
                "createdBy": {
                  "userId": "z1869",
                  "userName": "Huang, Xin",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z3227",
                  "userName": "Villegas, Mark",
                  "role": "GEMS SUPER USER"
                },
                "id": "001",
                "name": "001",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 1111111111,
                "approverJunketMin": 0,
                "approverJunketMax": 2222222222
              },
              {
                "createdDate": "2021-03-22T17:03:17.186+0800",
                "lastModifiedDate": "2021-03-22T17:03:17.186+0800",
                "createdBy": {
                  "userId": "z1869",
                  "userName": "Huang, Xin",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z1869",
                  "userName": "Huang, Xin",
                  "role": "GEMS SUPER USER"
                },
                "id": "002",
                "name": "002",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approverMin": 1,
                "approverMax": 999999999,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 1,
                    "approverMax": 999999999
                  }
                ]
              },
              {
                "createdDate": "2021-03-22T17:05:34.345+0800",
                "lastModifiedDate": "2021-03-22T17:05:34.345+0800",
                "createdBy": {
                  "userId": "z1869",
                  "userName": "Huang, Xin",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z1869",
                  "userName": "Huang, Xin",
                  "role": "GEMS SUPER USER"
                },
                "id": "003",
                "name": "003",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 1,
                "approverMax": 9999999999,
                "approverJunketMin": 1,
                "approverJunketMax": 99999999999,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 1,
                    "approverMax": 9999999999,
                    "approverJunketMin": 1,
                    "approverJunketMax": 99999999999
                  }
                ]
              },
              {
                "createdDate": "2021-03-22T17:08:11.776+0800",
                "lastModifiedDate": "2021-11-11T15:52:31.324+0800",
                "createdBy": {
                  "userId": "z1869",
                  "userName": "Huang, Xin",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2435",
                  "userName": "Mauricio, Don",
                  "role": "GEMS SUPER USER"
                },
                "id": "004",
                "name": "004",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": false,
                "approverMin": 1,
                "approverMax": 99999999999,
                "approverJunketMin": 1,
                "approverJunketMax": 1
              },
              {
                "createdDate": "2019-03-14T21:41:45.453+0800",
                "lastModifiedDate": "2019-10-25T15:17:38.764+0800",
                "createdBy": {
                  "userId": "38169",
                  "userName": "Tung, Ngok Fung",
                  "role": "G2 SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "47234",
                  "userName": "Chan, Chiu Ching",
                  "role": "GEMS SUPER USER"
                },
                "id": "38169",
                "name": "AML TESTER",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 300000
              },
              {
                "createdDate": "2019-03-11T16:09:06.845+0800",
                "lastModifiedDate": "2019-03-14T16:36:38.823+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "14845",
                "name": "ANLUTZ QAJF",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2021-09-28T16:51:07.940+0800",
                "lastModifiedDate": "2021-11-04T15:07:31.376+0800",
                "createdBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "id": "0927A2",
                "name": "BESY SIA",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999,
                "approverJunketMin": 0,
                "approverJunketMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:13:10.325+0800",
                "lastModifiedDate": "2019-03-11T16:13:10.325+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "45395",
                "name": "CCXJTM, YDOILYHCN",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:05:31.047+0800",
                "lastModifiedDate": "2019-03-14T16:33:35.239+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "18199",
                "name": "CZCCV ZEG",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:31:13.556+0800",
                "lastModifiedDate": "2019-03-14T14:15:13.926+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "88028",
                "name": "DLQXV, HTBBU ",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 1,
                "approverMax": 999999999999999,
                "approverJunketMin": 1,
                "approverJunketMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:12:20.199+0800",
                "lastModifiedDate": "2019-03-14T16:39:32.475+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "314",
                "name": "DQOWYFY BSQXA",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2021-09-28T16:55:40.194+0800",
                "lastModifiedDate": "2021-09-28T16:55:40.194+0800",
                "createdBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "id": "0927B3",
                "name": "Donald Tateish ",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 999999999999999,
                "approverJunketMin": 0,
                "approverJunketMax": 999999999999999,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  },
                  {
                    "currency": "PHP",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  }
                ]
              },
              {
                "createdDate": "2019-03-11T16:11:49.820+0800",
                "lastModifiedDate": "2019-03-14T16:39:01.880+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "P005376",
                "name": "EMDYN XKFPJSNEY",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2021-09-28T16:56:25.227+0800",
                "lastModifiedDate": "2021-09-28T16:56:25.227+0800",
                "createdBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "id": "0927C2",
                "name": "Evan Winkler",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 999999999999999,
                "approverJunketMin": 0,
                "approverJunketMax": 999999999999999,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  },
                  {
                    "currency": "PHP",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  }
                ]
              },
              {
                "createdDate": "2019-07-10T15:36:28.330+0800",
                "lastModifiedDate": "2019-07-10T15:36:28.330+0800",
                "createdBy": {
                  "userId": "z2153",
                  "userName": "Tang, Hayden",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2153",
                  "userName": "Tang, Hayden",
                  "role": "GEMS SUPER USER"
                },
                "id": "111",
                "name": "FDU",
                "active": true,
                "endorser": false,
                "approver": false,
                "approverJunket": true,
                "approverMin": 1,
                "approverMax": 2,
                "approverJunketMin": 1,
                "approverJunketMax": 2
              },
              {
                "createdDate": "2019-03-11T16:09:29.280+0800",
                "lastModifiedDate": "2019-03-14T16:36:59.270+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "19613",
                "name": "GKPKNYR RSM",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2021-03-22T18:09:41.285+0800",
                "lastModifiedDate": "2021-03-22T18:09:41.285+0800",
                "createdBy": {
                  "userId": "24939",
                  "userName": "Cheong, Chi Pang",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "24939",
                  "userName": "Cheong, Chi Pang",
                  "role": "GEMS SUPER USER"
                },
                "id": "24939",
                "name": "GORDON ",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approverMin": 1,
                "approverMax": 10000000000000000,
                "approverJunketMin": 14,
                "approverJunketMax": 1000000000000000000,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 1,
                    "approverMax": 10000000000000000,
                    "approverJunketMin": 14,
                    "approverJunketMax": 1000000000000000000
                  }
                ]
              },
              {
                "createdDate": "2021-09-28T16:56:03.928+0800",
                "lastModifiedDate": "2021-09-28T16:56:03.928+0800",
                "createdBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "id": "0927C1",
                "name": "Geoffrey Davis",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 999999999999999,
                "approverJunketMin": 0,
                "approverJunketMax": 999999999999999,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  },
                  {
                    "currency": "PHP",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  }
                ]
              },
              {
                "createdDate": "2019-03-11T16:32:51.401+0800",
                "lastModifiedDate": "2019-03-25T10:58:38.644+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "38209",
                "name": "HEETC, TPQ",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 999999999999999,
                "approverJunketMin": 1,
                "approverJunketMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:31:36.448+0800",
                "lastModifiedDate": "2019-03-14T14:15:23.708+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "24",
                "name": "HVJFCXI, ILCO  ",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 1,
                "approverMax": 999999999999999,
                "approverJunketMin": 1,
                "approverJunketMax": 999999999999999
              },
              {
                "createdDate": "2021-09-28T16:55:03.822+0800",
                "lastModifiedDate": "2021-09-28T16:55:03.822+0800",
                "createdBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "id": "0927B2",
                "name": "Jason Lamson",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 999999999999999,
                "approverJunketMin": 0,
                "approverJunketMax": 999999999999999,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  },
                  {
                    "currency": "PHP",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  }
                ]
              },
              {
                "createdDate": "2021-09-28T16:52:23.900+0800",
                "lastModifiedDate": "2021-09-28T16:52:23.900+0800",
                "createdBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "id": "0927A3",
                "name": "Joshua Clements",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 999999999999999,
                "approverJunketMin": 0,
                "approverJunketMax": 999999999999999,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 0,
                    "approverMax": 4999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 4999999
                  },
                  {
                    "currency": "PHP",
                    "approverMin": 0,
                    "approverMax": 32499999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 32499999
                  }
                ]
              },
              {
                "createdDate": "2021-11-04T16:59:10.770+0800",
                "lastModifiedDate": "2021-11-04T16:59:10.770+0800",
                "createdBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "id": "z2849",
                "name": "LAKHI C",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 0,
                "approverJunketMin": 0,
                "approverJunketMax": 0,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  },
                  {
                    "currency": "PHP",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  }
                ]
              },
              {
                "createdDate": "2019-03-11T16:06:24.047+0800",
                "lastModifiedDate": "2019-03-14T16:34:37.875+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "16622",
                "name": "LDYIF CIA",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:13:47.322+0800",
                "lastModifiedDate": "2019-03-14T16:41:38.755+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "16845",
                "name": "LGSZEM WOHD",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:07:19.202+0800",
                "lastModifiedDate": "2019-03-14T16:35:18.991+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "1708",
                "name": "LKNDJ ECCX",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:30:29.692+0800",
                "lastModifiedDate": "2019-03-14T16:24:31.545+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "4392",
                "name": "LLACAI FLMI",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approverMin": 1,
                "approverMax": 10000000,
                "approverJunketMin": 1,
                "approverJunketMax": 10000000
              },
              {
                "createdDate": "2021-09-28T16:56:43.389+0800",
                "lastModifiedDate": "2021-09-28T16:56:43.389+0800",
                "createdBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "id": "0927D1",
                "name": "Lawrence Ho",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 999999999999999,
                "approverJunketMin": 0,
                "approverJunketMax": 999999999999999,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  },
                  {
                    "currency": "PHP",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  }
                ]
              },
              {
                "createdDate": "2021-11-12T10:07:05.089+0800",
                "lastModifiedDate": "2021-11-12T10:07:05.089+0800",
                "createdBy": {
                  "userId": "z2435",
                  "userName": "Mauricio, Don",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2435",
                  "userName": "Mauricio, Don",
                  "role": "GEMS SUPER USER"
                },
                "id": "z2435",
                "name": "MAURICIO, DON",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approverMin": 1,
                "approverMax": 9999999,
                "approverJunketMin": 1,
                "approverJunketMax": 9999999
              },
              {
                "createdDate": "2021-11-12T13:20:04.765+0800",
                "lastModifiedDate": "2021-11-12T13:20:04.765+0800",
                "createdBy": {
                  "userId": "z2435",
                  "userName": "Mauricio, Don",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2435",
                  "userName": "Mauricio, Don",
                  "role": "GEMS SUPER USER"
                },
                "id": "2435",
                "name": "MAURICIO, DON",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 0,
                    "approverMax": 9999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 9999999
                  }
                ]
              },
              {
                "createdDate": "2019-03-11T16:32:01.489+0800",
                "lastModifiedDate": "2019-03-14T14:15:36.346+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "8888",
                "name": "MB, ZCHCXIXX ",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 1,
                "approverMax": 999999999999999,
                "approverJunketMin": 1,
                "approverJunketMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:12:42.596+0800",
                "lastModifiedDate": "2019-03-14T16:39:54.236+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "19512",
                "name": "MHETVFM NUVT",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:07:55.123+0800",
                "lastModifiedDate": "2019-03-14T16:35:39.354+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "16875",
                "name": "MTYOYQD QRZIZBJBFK",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2021-09-28T16:50:09.363+0800",
                "lastModifiedDate": "2021-09-28T16:50:30.006+0800",
                "createdBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "id": "0927A1",
                "name": "Maggie Chio",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 999999999999999,
                "approverJunketMin": 0,
                "approverJunketMax": 999999999999999,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 0,
                    "approverMax": 4999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 4999999
                  },
                  {
                    "currency": "PHP",
                    "approverMin": 0,
                    "approverMax": 32499999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 32499999
                  }
                ]
              },
              {
                "createdDate": "2021-11-12T13:52:47.744+0800",
                "lastModifiedDate": "2021-11-12T13:58:07.604+0800",
                "createdBy": {
                  "userId": "z3227",
                  "userName": "Villegas, Mark",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z3227",
                  "userName": "Villegas, Mark",
                  "role": "GEMS SUPER USER"
                },
                "id": "212121",
                "name": "Mark Villegas",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 0,
                    "approverMax": 9999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 9999999
                  },
                  {
                    "currency": "PHP",
                    "approverMin": 0,
                    "approverMax": 9999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 9999999
                  }
                ]
              },
              {
                "createdDate": "2021-11-12T22:01:34.030+0800",
                "lastModifiedDate": "2021-11-12T22:01:34.030+0800",
                "createdBy": {
                  "userId": "z3227",
                  "userName": "Villegas, Mark",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z3227",
                  "userName": "Villegas, Mark",
                  "role": "GEMS SUPER USER"
                },
                "id": "55555",
                "name": "Mark Villegas",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 100000000,
                    "approverMax": 200000000,
                    "approverJunketMin": 100000000,
                    "approverJunketMax": 200000000
                  },
                  {
                    "currency": "PHP",
                    "approverMin": 100000000,
                    "approverMax": 200000000,
                    "approverJunketMin": 100000000,
                    "approverJunketMax": 200000000
                  }
                ]
              },
              {
                "createdDate": "2019-03-11T16:11:27.761+0800",
                "lastModifiedDate": "2019-03-14T16:38:36.400+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "14859",
                "name": "NQEXU PQHDN",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:33:15.457+0800",
                "lastModifiedDate": "2019-03-25T10:58:52.078+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "633",
                "name": "OSC, HZFX",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": false,
                "approverMin": 1,
                "approverMax": 5000000
              },
              {
                "createdDate": "2021-09-28T16:53:11.333+0800",
                "lastModifiedDate": "2021-09-28T16:53:11.333+0800",
                "createdBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "id": "0927B1",
                "name": "Phil Geappen",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 999999999999999,
                "approverJunketMin": 0,
                "approverJunketMax": 999999999999999,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  },
                  {
                    "currency": "PHP",
                    "approverMin": 0,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 999999999999999
                  }
                ]
              },
              {
                "createdDate": "2019-03-11T16:06:55.691+0800",
                "lastModifiedDate": "2019-03-14T16:34:57.576+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "16911",
                "name": "SETMT I QB",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:08:45.570+0800",
                "lastModifiedDate": "2019-03-14T16:36:20.868+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "16930",
                "name": "SLZXK ZCCT",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2021-09-28T16:52:39.536+0800",
                "lastModifiedDate": "2021-09-28T16:52:39.536+0800",
                "createdBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z2849",
                  "userName": "Calantoc, Lakhi",
                  "role": "GEMS SUPER USER"
                },
                "id": "0927A4",
                "name": "Stewart Coutts",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 0,
                "approverMax": 999999999999999,
                "approverJunketMin": 0,
                "approverJunketMax": 999999999999999,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 0,
                    "approverMax": 4999999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 4999999
                  },
                  {
                    "currency": "PHP",
                    "approverMin": 0,
                    "approverMax": 32499999,
                    "approverJunketMin": 0,
                    "approverJunketMax": 32499999
                  }
                ]
              },
              {
                "createdDate": "2021-11-11T23:37:25.371+0800",
                "lastModifiedDate": "2021-11-11T23:37:25.371+0800",
                "createdBy": {
                  "userId": "z3227",
                  "userName": "Villegas, Mark",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z3227",
                  "userName": "Villegas, Mark",
                  "role": "GEMS SUPER USER"
                },
                "id": "123456",
                "name": "TEST01",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approverMin": 100000000,
                "approverMax": 200000000,
                "approverJunketMin": 100000000,
                "approverJunketMax": 200000000
              },
              {
                "createdDate": "2021-11-11T23:45:26.614+0800",
                "lastModifiedDate": "2021-11-11T23:45:26.614+0800",
                "createdBy": {
                  "userId": "z3227",
                  "userName": "Villegas, Mark",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z3227",
                  "userName": "Villegas, Mark",
                  "role": "GEMS SUPER USER"
                },
                "id": "11111",
                "name": "TEST02",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approverMin": 100000000,
                "approverMax": 200000000,
                "approverJunketMin": 100000000,
                "approverJunketMax": 200000000
              },
              {
                "createdDate": "2021-11-11T23:55:33.561+0800",
                "lastModifiedDate": "2021-11-11T23:55:33.561+0800",
                "createdBy": {
                  "userId": "z3227",
                  "userName": "Villegas, Mark",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "z3227",
                  "userName": "Villegas, Mark",
                  "role": "GEMS SUPER USER"
                },
                "id": "22222",
                "name": "TEST03",
                "active": true,
                "endorser": true,
                "approver": true,
                "approverJunket": true,
                "approverMin": 100000000,
                "approverMax": 200000000,
                "approverJunketMin": 100000000,
                "approverJunketMax": 200000000
              },
              {
                "createdDate": "2019-03-11T16:08:21.537+0800",
                "lastModifiedDate": "2019-03-14T16:36:00.355+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "16907",
                "name": "TLINXW STO",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:04:05.194+0800",
                "lastModifiedDate": "2019-03-14T16:33:03.645+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "20291",
                "name": "UG BC",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": true,
                "approverMin": 1,
                "approverMax": 999999999999999,
                "approvers": [
                  {
                    "currency": "HKD",
                    "approverMin": 1,
                    "approverMax": 999999999999999,
                    "approverJunketMin": 1,
                    "approverJunketMax": 999999999999999
                  }
                ]
              },
              {
                "createdDate": "2019-03-11T16:30:52.260+0800",
                "lastModifiedDate": "2019-03-25T10:59:03.218+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "38615",
                "name": "UTLDBD, QABGAIM BPRRGV  ",
                "active": true,
                "endorser": false,
                "approver": true,
                "approverJunket": false,
                "approverMin": 1,
                "approverMax": 5000000
              },
              {
                "createdDate": "2019-03-11T16:06:01.421+0800",
                "lastModifiedDate": "2019-03-14T16:34:18.631+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "17381",
                "name": "VRWPL RKS",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:11:05.283+0800",
                "lastModifiedDate": "2019-03-14T16:38:18.254+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "36093",
                "name": "WYGLCA OM",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:10:41.744+0800",
                "lastModifiedDate": "2019-03-14T16:37:49.197+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "28472",
                "name": "WYVRH CAJFG",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:09:54.324+0800",
                "lastModifiedDate": "2019-03-14T16:37:18.396+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "1733",
                "name": "XTCKT IKCW",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              },
              {
                "createdDate": "2019-03-11T16:10:16.461+0800",
                "lastModifiedDate": "2019-03-14T16:37:36.144+0800",
                "createdBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "lastModifiedBy": {
                  "userId": "46371",
                  "userName": "Law, Ching Yi",
                  "role": "GEMS SUPER USER"
                },
                "id": "15101",
                "name": "ZKFEGQAF CNOT",
                "active": true,
                "endorser": true,
                "approver": false,
                "approverJunket": false,
                "approverMin": 0,
                "approverMax": 999999999999999
              }
            ]
          });
    }
}
