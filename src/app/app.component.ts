import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [AppService]
})

export class AppComponent implements OnInit {
  public authMemberData: any = [];
  public selectedMember: any;
  public isOpenAuthMemberDialog: boolean = false;
  public authMemberDialogType: string;
  public approversData: any = [];
  public selectedApproverConfig: any;
  public authMemberForm: FormGroup;
  public isOpenApproverConfig: boolean = false;
  public approverConfigType: string;
  public approvers: FormGroup;
  public tabViewIndex: number = 0;
  public approverConfigRowIndex: number;

  constructor(
    private appService: AppService,
    private fb: FormBuilder
  ) { }

  public ngOnInit(): void {
    this.authMemberForm = this.fb.group({
      id: ['', Validators.required],
      name: ['', Validators.required],
      endorser: ['', Validators.required],
      approver: ['', Validators.required],
      approverJunket: ['', Validators.required],
      approvers: this.fb.group({
        currency: [''],
        approverMin: [''],
        approverMax: [''],
        approverJunketMin: [''],
        approverJunketMax: ['']
      })
    });

    this.approvers = this.authMemberForm.get('approvers') as FormGroup;

    this.initAuthMember();
  }

  public initAuthMember(): void {
    this.appService.getAuthMember()
      .subscribe(data => {
        console.log(data, 'data')
        this.authMemberData = data.data;
      });
  }

  public onRowSelectMember(): void {
    if ((this.selectedMember.approvers !== undefined) && this.selectedMember.approvers.length) {
      this.approversData = this.selectedMember.approvers;
    } else {
      this.approversData = [];
    }
  }

  public clickAuthMemberDialog(type: string) {
    this.authMemberDialogType = type;
    this.isOpenAuthMemberDialog = true;
    this.selectedApproverConfig = null;
    this.tabViewIndex = 0;

    if (this.authMemberDialogType === 'add') {
      this.selectedMember = null;
      this.approversData = [];
      this.authMemberForm.controls.id.setValue(null);
      this.authMemberForm.controls.name.setValue(null);
      this.authMemberForm.controls.endorser.setValue(false);
      this.authMemberForm.controls.approver.setValue(false);
      this.authMemberForm.controls.approverJunket.setValue(false);

    } else if (this.authMemberDialogType === 'modify') {
      this.authMemberForm.controls.id.setValue(this.selectedMember.id);
      this.authMemberForm.controls.name.setValue(this.selectedMember.name);
      this.authMemberForm.controls.endorser.setValue(this.selectedMember.endorser);
      this.authMemberForm.controls.approver.setValue(this.selectedMember.approver);
      this.authMemberForm.controls.approverJunket.setValue(this.selectedMember.approverJunket);
    }
  }

  public onRowSelectApproverConfig(event: any): void {
    this.approverConfigRowIndex = event.index;
  }

  public clickApproverConfigDialog(type: string): void {
    this.approverConfigType = type;
    this.isOpenApproverConfig = true;

    this.isApproverConfigValidatorsRequired(true);

    if (this.approverConfigType === 'add') {
      this.selectedApproverConfig = null;
      this.approvers.controls.currency.setValue(null);
      this.approvers.controls.approverMin.setValue(null);
      this.approvers.controls.approverMax.setValue(null);
      this.approvers.controls.approverJunketMin.setValue(null);
      this.approvers.controls.approverJunketMax.setValue(null);

    } else if (this.approverConfigType === 'modify') {
      this.approvers.controls.currency.setValue(this.selectedApproverConfig.currency);
      this.approvers.controls.approverMin.setValue(this.selectedApproverConfig.approverMin);
      this.approvers.controls.approverMax.setValue(this.selectedApproverConfig.approverMax);
      this.approvers.controls.approverJunketMin.setValue(this.selectedApproverConfig.approverJunketMin);
      this.approvers.controls.approverJunketMax.setValue(this.selectedApproverConfig.approverJunketMax);

    } else if (this.approverConfigType === 'delete') {
      this.approverConfigType = 'delete';
      this.submitApproverConfig();
    }
  }

  public submitAuthMember(): void {
    const params = {
      id: this.authMemberForm.controls.id.value,
      name: this.authMemberForm.controls.name.value,
      endorser: this.authMemberForm.controls.endorser.value,
      approver: this.authMemberForm.controls.approver.value,
      approverJunket: this.authMemberForm.controls.approverJunket.value,
      approvers: this.approversData
    };

    console.log(params, 'submitAuthMember');
  }

  public submitApproverConfig(): void {
    if (this.approverConfigType === 'add') {
      this.approversData.push(this.approvers.value);

    } else if (this.approverConfigType === 'modify') {
      this.approversData[this.approverConfigRowIndex] = this.approvers.value;
      this.selectedApproverConfig = null;

    } else if (this.approverConfigType === 'delete') {
      this.approversData.splice(this.approverConfigRowIndex, 1);
    }

    this.isOpenApproverConfig = false;
  }

  public cancelApproverConfig(): void {
    this.isApproverConfigValidatorsRequired(false);
  }

  public cancelAuthMember(): void {
    this.approversData = [];
    this.selectedMember = null;
    this.initAuthMember();
  }

  public isApproverConfigValidatorsRequired(isRequired: boolean): void {
    Object.keys(this.approvers.controls).forEach(key => {
      if (isRequired) {
        this.approvers.controls[key].setValidators(Validators.required);
      } else {
        this.approvers.controls[key].clearValidators();
        this.approvers.controls[key].updateValueAndValidity();
      }
    });
  }
}
